Projeto do Campeonato Paulista 2016 para a Matéria **Laboratório de Banco de Dados**
do curso de Análise e Desenvolvimento de Sistemas da FATEC ZL.

Professor M.Sc.: **Leandro Colevati dos Santos**

Tarde - 2º Semestre 2016

- 26/09/2016 [AVALIAÇÃO 1](https://bitbucket.org/fatec2016/campeonatopaulista/wiki/Fase%2001%20-%20Intro): Grupos e Jogos 

- 31/10/2016 [AVALIAÇÃO 2](https://bitbucket.org/fatec2016/campeonatopaulista/wiki/Fase%2002%20-%20Intro): Pontuação, Rebaixamento e 4ª de Final

- 28/11/2016 [AVALIAÇÃO 3](https://bitbucket.org/fatec2016/campeonatopaulista/wiki/Fase%2003%20-%20Intro): Backup e Restore da Base de Dados



Projeto realizado por: **FERNANDO MORAES OLVIEIRA**


O histórico e a documentação podem ser acompanhados no [**wiki** do projeto](https://bitbucket.org/fatec2016/campeonatopaulista/wiki/).

O **script do banco de dados** deste sistema também se encontra na pasta "sql" (LabBD_Avaliacao_1.sql) do projeto.

**OBS.:** Para a conexão no banco, trocar o usuário e senha do Microsoft SQL Server
na Classe GenericDao (package "persistence").

![Captura de tela 2016-11-26 16.47.13.png](https://bitbucket.org/repo/zLrx6z/images/1696439213-Captura%20de%20tela%202016-11-26%2016.47.13.png)